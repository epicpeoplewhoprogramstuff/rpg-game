#ifndef HPP_DEATHINTERFACE_INCLUDED
#define HPP_DEATHINTERFACE_INCLUDED

#include <iostream>

#include "../Database/Database.hpp"

using namespace std;

class DeathInterface
{
	public:
		DeathInterface(Database *database);
		void runDeathInterface();
		~DeathInterface();

	private:
		Database *database;
	
};

#endif // HPP_DEATHINTERFACE_INCLUDED
