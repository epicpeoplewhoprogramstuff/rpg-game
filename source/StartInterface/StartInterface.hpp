#ifndef STARTINTERFACE_HPP_INCLUDED
#define STARTINTERFACE_HPP_INCLUDED

#include <iostream>

#include "BarfHandler/BarfHandler.hpp"

#include "../Database/Database.hpp"

using namespace std;

class StartInterface
{
	public:
		StartInterface(Database *database);
		void runStartInterface();
		~StartInterface();

	private:
		Database *database;
		
};

#endif // STARTINTERFACE_HPP_INCLUDED
