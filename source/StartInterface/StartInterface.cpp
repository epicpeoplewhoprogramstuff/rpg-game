#include "StartInterface.hpp"

StartInterface::StartInterface(Database *database)
{
	//assign the database pointer stored under private in this object from the pointer in the arguments of this function
	this -> database = database;
}

void StartInterface::runStartInterface()
{
	//Swear at user...
	cout << "Welcome Brave Adventurer!\n";
	cout << "What is your name? ";
	database -> name = "";
	cin >> database -> name;
	database -> name += " of Obscure Unimportant Kingdom";
	cout << "Well then, hello brave " << database -> name  << " are you ready to begin your quest? ";
	string ready;
	cin >> ready;
	
	if (ready == "No" || ready == "no" || ready == "derp")
	{
		cout << "What do you mean NO?  #$&*@ YOU! UNGRATEFUL KID! BAH!\n";
		cout << "Congrats, You Lose! :D\n";
		cout << "LLLLLLLLLLLLOOOOOOOOOOOSSSSSSSSSSSSSEEEEEEEEEEEEERRRRRRRRRRRR! XD XD XD XD XD XD XD\n";
		database -> health = 0;
		return;
	}
	else if(ready == "barf" || ready == "Barf")
	{
		BarfHandler barfHandler;
		barfHandler.handleBarf();

	}

	cout << "Well then, let's start you off with... 5 points, shall we?\n";
	cout << "Haha just kidding that hasn't been implimented yet...\n";
	cout << "Actually ok whatever it has so here you go.\n";
	database -> printIntro();
	database -> addPoints(10);
	cout << "Throughout your quest you will encounter many different enemies and challenges, be warned, you can lose your points by doing the wrong thing.\n";
	cout << "You will die if your health gets depleted.\nHealth: " << database -> health << "\n";
	cout << "Now, let's begin the adventure!\nYou start off in the town of Bree.\nThere are many people milling about and doing business.\n";
	cout << "North is Farmer JoJo's shop, to the East is Bob's Bar, to the West is Uncle Charlie's Fish and Chips, and to the South is the town entrance.\nWhere do you want to go?";

}

StartInterface::~StartInterface()
{
}
