#ifndef HPP_BARFHANDLER_INCLUDED
#define HPP_BARFHANDLER_INCLUDED

#include <iostream>

using namespace std;

class BarfHandler
{
	public:
		BarfHandler();
		void handleBarf();
		~BarfHandler();

};

#endif // HPP_BARFHANDLER_INCLUDED
