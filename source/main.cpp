#include <stdio.h>

#include "StartInterface/StartInterface.hpp"
#include "ActionInterface/ActionInterface.hpp"
#include "DeathInterface/DeathInterface.hpp"

int main(int argc, char **argv)
{
	Database database;
	StartInterface startInterface(&database);
	ActionInterface actionInterface(&database);
	DeathInterface deathInterface(&database);
	
	
	while(database.running)
	{
		startInterface.runStartInterface();
		
		while(database.health > 0)
			actionInterface.run();		
		
		deathInterface.runDeathInterface();
	}
	
	
	return 0;
}
