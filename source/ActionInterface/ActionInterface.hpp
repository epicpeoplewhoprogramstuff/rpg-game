#ifndef HPP_ACTIONINTERFACE_INCLUDED
#define HPP_ACTIONINTERFACE_INCLUDED

#include <iostream>

#include "../Database/Database.hpp"

using namespace std;

class ActionInterface
{
	public:
		ActionInterface(Database *database);
		void run();
		~ActionInterface();

	private:
		Database *database;

};

#endif // HPP_ACTIONINTERFACE_INCLUDED
