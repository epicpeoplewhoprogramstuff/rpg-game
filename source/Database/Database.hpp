#ifndef HPP_DATABASE_INCLUDED
#define HPP_DATABASE_INCLUDED

#include <iostream>

using namespace std;

class Database
{
	public:
		Database();
		void printIntro();
		void addPoints(int addPoints);
		~Database();
		
		string name;
		
		int health;
		int points;
		int coins;
		
		int town;
		
		bool running;
		
		string *townNames;

};

#endif // HPP_DATABASE_INCLUDED
