#include "Database.hpp"

Database::Database()
{
	running = true;
	health = 10;
}

void Database::printIntro()
{
	cout << "\tHey there I'm the database I store stuff like your health,\n";
	cout << "\tcoins, points, etc.\n";
}

void Database::addPoints(int addPoints)
{
	cout << "\tDatabase: \n";
	cout << "\tYou got some points (what a lucky fellow you are indeed).\n";
	cout << "\tOld points: " << points << ".\n";
	cout << "\tAmount of new points " << addPoints << ".\n";
	points += addPoints;
	cout << "\tYou now have " << points << " points good for you.\n";
}

Database::~Database()
{
}

