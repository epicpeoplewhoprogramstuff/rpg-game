##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=RPG_Game
ConfigurationName      :=Debug
WorkspacePath          := "/home/mitchell/.codelite/Mitchell"
ProjectPath            := "/home/mitchell/development/RPG_Game"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=Mitchell Marasch
Date                   :=05/29/14
CodeLitePath           :="/home/mitchell/.codelite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="RPG_Game.txt"
PCHCompileFlags        :=
MakeDirCommand         :=mkdir -p
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=/usr/share/codelite
Objects0=$(IntermediateDirectory)/source_main$(ObjectSuffix) $(IntermediateDirectory)/StartInterface_StartInterface$(ObjectSuffix) $(IntermediateDirectory)/Database_Database$(ObjectSuffix) $(IntermediateDirectory)/DeathInterface_DeathInterface$(ObjectSuffix) $(IntermediateDirectory)/ActionInterface_ActionInterface$(ObjectSuffix) $(IntermediateDirectory)/BarfHandler_BarfHandler$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@test -d ./Debug || $(MakeDirCommand) ./Debug

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/source_main$(ObjectSuffix): source/main.cpp $(IntermediateDirectory)/source_main$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/mitchell/development/RPG_Game/source/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/source_main$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/source_main$(DependSuffix): source/main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/source_main$(ObjectSuffix) -MF$(IntermediateDirectory)/source_main$(DependSuffix) -MM "source/main.cpp"

$(IntermediateDirectory)/source_main$(PreprocessSuffix): source/main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/source_main$(PreprocessSuffix) "source/main.cpp"

$(IntermediateDirectory)/StartInterface_StartInterface$(ObjectSuffix): source/StartInterface/StartInterface.cpp $(IntermediateDirectory)/StartInterface_StartInterface$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/mitchell/development/RPG_Game/source/StartInterface/StartInterface.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/StartInterface_StartInterface$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/StartInterface_StartInterface$(DependSuffix): source/StartInterface/StartInterface.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/StartInterface_StartInterface$(ObjectSuffix) -MF$(IntermediateDirectory)/StartInterface_StartInterface$(DependSuffix) -MM "source/StartInterface/StartInterface.cpp"

$(IntermediateDirectory)/StartInterface_StartInterface$(PreprocessSuffix): source/StartInterface/StartInterface.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/StartInterface_StartInterface$(PreprocessSuffix) "source/StartInterface/StartInterface.cpp"

$(IntermediateDirectory)/Database_Database$(ObjectSuffix): source/Database/Database.cpp $(IntermediateDirectory)/Database_Database$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/mitchell/development/RPG_Game/source/Database/Database.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Database_Database$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Database_Database$(DependSuffix): source/Database/Database.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Database_Database$(ObjectSuffix) -MF$(IntermediateDirectory)/Database_Database$(DependSuffix) -MM "source/Database/Database.cpp"

$(IntermediateDirectory)/Database_Database$(PreprocessSuffix): source/Database/Database.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Database_Database$(PreprocessSuffix) "source/Database/Database.cpp"

$(IntermediateDirectory)/DeathInterface_DeathInterface$(ObjectSuffix): source/DeathInterface/DeathInterface.cpp $(IntermediateDirectory)/DeathInterface_DeathInterface$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/mitchell/development/RPG_Game/source/DeathInterface/DeathInterface.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/DeathInterface_DeathInterface$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/DeathInterface_DeathInterface$(DependSuffix): source/DeathInterface/DeathInterface.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/DeathInterface_DeathInterface$(ObjectSuffix) -MF$(IntermediateDirectory)/DeathInterface_DeathInterface$(DependSuffix) -MM "source/DeathInterface/DeathInterface.cpp"

$(IntermediateDirectory)/DeathInterface_DeathInterface$(PreprocessSuffix): source/DeathInterface/DeathInterface.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/DeathInterface_DeathInterface$(PreprocessSuffix) "source/DeathInterface/DeathInterface.cpp"

$(IntermediateDirectory)/ActionInterface_ActionInterface$(ObjectSuffix): source/ActionInterface/ActionInterface.cpp $(IntermediateDirectory)/ActionInterface_ActionInterface$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/mitchell/development/RPG_Game/source/ActionInterface/ActionInterface.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ActionInterface_ActionInterface$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ActionInterface_ActionInterface$(DependSuffix): source/ActionInterface/ActionInterface.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/ActionInterface_ActionInterface$(ObjectSuffix) -MF$(IntermediateDirectory)/ActionInterface_ActionInterface$(DependSuffix) -MM "source/ActionInterface/ActionInterface.cpp"

$(IntermediateDirectory)/ActionInterface_ActionInterface$(PreprocessSuffix): source/ActionInterface/ActionInterface.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ActionInterface_ActionInterface$(PreprocessSuffix) "source/ActionInterface/ActionInterface.cpp"

$(IntermediateDirectory)/BarfHandler_BarfHandler$(ObjectSuffix): source/StartInterface/BarfHandler/BarfHandler.cpp $(IntermediateDirectory)/BarfHandler_BarfHandler$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "/home/mitchell/development/RPG_Game/source/StartInterface/BarfHandler/BarfHandler.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/BarfHandler_BarfHandler$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/BarfHandler_BarfHandler$(DependSuffix): source/StartInterface/BarfHandler/BarfHandler.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/BarfHandler_BarfHandler$(ObjectSuffix) -MF$(IntermediateDirectory)/BarfHandler_BarfHandler$(DependSuffix) -MM "source/StartInterface/BarfHandler/BarfHandler.cpp"

$(IntermediateDirectory)/BarfHandler_BarfHandler$(PreprocessSuffix): source/StartInterface/BarfHandler/BarfHandler.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/BarfHandler_BarfHandler$(PreprocessSuffix) "source/StartInterface/BarfHandler/BarfHandler.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/source_main$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/source_main$(DependSuffix)
	$(RM) $(IntermediateDirectory)/source_main$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/StartInterface_StartInterface$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/StartInterface_StartInterface$(DependSuffix)
	$(RM) $(IntermediateDirectory)/StartInterface_StartInterface$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/Database_Database$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Database_Database$(DependSuffix)
	$(RM) $(IntermediateDirectory)/Database_Database$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/DeathInterface_DeathInterface$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/DeathInterface_DeathInterface$(DependSuffix)
	$(RM) $(IntermediateDirectory)/DeathInterface_DeathInterface$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/ActionInterface_ActionInterface$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ActionInterface_ActionInterface$(DependSuffix)
	$(RM) $(IntermediateDirectory)/ActionInterface_ActionInterface$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/BarfHandler_BarfHandler$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/BarfHandler_BarfHandler$(DependSuffix)
	$(RM) $(IntermediateDirectory)/BarfHandler_BarfHandler$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) "../../.codelite/Mitchell/.build-debug/RPG_Game"


